package ru.tsc.fuksina.tm.api.controller;

public interface ITaskController {

    void showTaskList();

    void clearTasks();

    void createTask();

    void removeTaskByIndex();

    void removeTaskById();

    void showTaskByIndex();

    void showTaskById();

    void updateTaskByIndex();

    void updateTaskById();

}
